`timescale 1ns / 1ps

module main(
    // 12MHz System Clock
    input wire clk,
    input wire [1:0] btn,
    output wire [2:0] vga_color,
    output wire [1:0] vga_sync,
    output wire [3:0] led
);
    wire [17:0] vga_buffer_address_read_inter;
    wire [17:0] vga_buffer_address_write_inter;
    wire [2:0] vga_buffer_in_data_inter;
    wire [2:0] vga_buffer_out_data_inter;
    
    wire [28:0] memory_data_inter;
    wire [15:0] memory_address_inter;
    
    
    vga(
        .clk(clk),
        .address(vga_buffer_address_read_inter),
        .in_color(vga_buffer_out_data_inter),
        .out_color(vga_color),
        .sync(vga_sync)
    );

    vga_buffer(
        .clk(clk),
        .address_read(vga_buffer_address_read_inter),
        .address_write(vga_buffer_address_write_inter),
        .in_data(vga_buffer_in_data_inter),
        .out_data(vga_buffer_out_data_inter)
    );
    
    
    memory(
        .clk(clk),
        .address(memory_address_inter),
        .data(memory_data_inter)
    );

    cpu(
        .clk(clk),
        .playerA(btn),
        .vga_buffer_address(vga_buffer_address_write_inter),
        .vga_buffer_data(vga_buffer_in_data_inter),
        .memory_address(memory_address_inter),
        .memory_data(memory_data_inter)
    );
endmodule
