`timescale 1ns / 1ps

module vga(
    input wire clk,
    // r, g, b
    input wire [2:0] in_color,
    output wire [2:0] out_color,
    // vsync, hsync
    output wire [1:0] sync,
    output wire [17:0] address
);

    localparam MAX_PIXEL = 12600000;
    reg [$clog2(MAX_PIXEL-1)-1:0] pixel_count = 'b0;

    localparam MAX_WIDTH = 400;
    reg [$clog2(MAX_WIDTH-1)-1:0] width_idx = 'b0;

    localparam MAX_HEIGHT = 525;
    reg [$clog2(MAX_HEIGHT-1)-1:0] height_idx = 'b0;

    reg active = 1'b0;

    reg vsync = 1'b0;
    reg hsync = 1'b0;

    always@(posedge clk) begin
        if (width_idx >= MAX_WIDTH-1)  begin
            width_idx <= 'b0;
            if (height_idx >= MAX_HEIGHT-1)  begin
                height_idx <= 'b0;
            end else
                height_idx <= height_idx + 1'b1;
        end else
            width_idx <= width_idx + 1'b1;

        if (width_idx < 328)
            hsync <= 1'b0;
        else if (width_idx > 376)
            hsync <= 1'b0;
        else
            hsync <= 1'b1;

        if (height_idx < 490)
            vsync <= 1'b0;
        else if (height_idx > 492)
            vsync <= 1'b0;
        else
            vsync <= 1'b1;

        if (width_idx < 320 & height_idx < 480)
            active <= 1'b1;
        else
            active <= 1'b0;
    end

    assign address[17:0] = {height_idx[8:0], width_idx[8:0]};

    assign out_color[0] = in_color[0] & active;
    assign out_color[1] = in_color[1] & active;
    assign out_color[2] = in_color[2] & active;

    assign sync[0] = vsync;
    assign sync[1]= hsync;


endmodule
