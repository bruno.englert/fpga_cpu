`timescale 1ns / 1ps


module cpu(
    input wire clk,
    input wire [1:0] playerA,
    // input wire [1:0] playerB,
    output wire [17:0] vga_buffer_address,
    output wire [2:0] vga_buffer_data,
    output wire [15:0] memory_address,
    input wire [28:0] memory_data
);

    parameter REG_SIZE = 16;
    reg [15:0] register [REG_SIZE-1:0];
    reg [15:0] pc = 16'b0;

    reg [2:0] color = 3'b0;
    reg [17:0] address;

    wire [3:0] inst;
    wire [3:0] op_dest;
    wire [3:0] op_left;
    wire [15:0] op_right;
    wire is_op_right_const;   
    
    assign memory_address = pc;
    assign {inst, op_dest, op_left, op_right, is_op_right_const} = memory_data;


    always@(posedge clk) begin
        register[15][1:0] = playerA;    
     
        case (inst)
            // 0. no op
            4'b0000:
            begin
                pc = pc + 1;
            end

            // 1. mov to video buffer
            4'b0001:
            begin
                address = {register[op_dest][8:0], register[op_left][8:0]};
                color = ((is_op_right_const==1'b1) ? op_right[2:0] : register[op_right[3:0]][2:0]);
                pc = pc + 1;
            end

            // 2. mov to reg
            4'b0010:
            begin
                register[op_dest] = ((is_op_right_const==1'b1) ? op_right : register[op_right[3:0]]);
                pc = pc + 1;
            end

            // 3. add
            4'b0011:
            begin
                register[op_dest] = register[op_left] + ((is_op_right_const==1'b1) ? op_right : register[op_right[3:0]]);
                pc = pc + 1;
            end

            // 4. sub
            4'b0100:
            begin
                register[op_dest] = register[op_left] - ((is_op_right_const==1'b1) ? op_right : register[op_right[3:0]]);
                pc = pc + 1;
            end

            // 5. mul
            4'b0101:
            begin
                register[op_dest] = register[op_left] * ((is_op_right_const==1'b1) ? op_right : register[op_right[3:0]]);
                pc = pc + 1;
            end

            // 6. div
            4'b0110:
            begin
                register[op_dest] = register[op_left] / ((is_op_right_const==1'b1) ? op_right : register[op_right[3:0]]);
                pc = pc + 1;
            end

            // 7. mod
            4'b0111:
            begin
                register[op_dest] = register[op_left] % ((is_op_right_const==1'b1) ? op_right : register[op_right[3:0]]);
                pc = pc + 1;
            end

            // 8. right shift
            4'b1000:
            begin
                register[op_dest] = register[op_left] >> ((is_op_right_const==1'b1) ? op_right : register[op_right[3:0]]);
                pc = pc + 1;
            end

            // 9. left shift
            4'b1001:
            begin
                register[op_dest] = register[op_left] << ((is_op_right_const==1'b1) ? op_right : register[op_right[3:0]]);
                pc = pc + 1;
            end

            // 10. jmp
            4'b1010:
            begin
                pc = (is_op_right_const==1'b1) ? op_right : register[op_right[3:0]];
            end

            // 11. jmp if equal
            4'b1011:
            begin
                if (register[op_left] == ((is_op_right_const==1'b1) ? op_right : register[op_right[3:0]]))
                    pc = register[op_dest];
                else
                    pc = pc + 1;
            end

            // 12. jmp if greater 
            4'b1100:
            begin
                if (register[op_left] < ((is_op_right_const==1'b1) ? op_right : register[op_right[3:0]]))
                    pc = register[op_dest];
                else
                    pc = pc + 1;
            end

            // 13. jmp if lower 
            4'b1101:
            begin
                if (register[op_left] > ((is_op_right_const==1'b1) ? op_right : register[op_right[3:0]]))
                    pc = register[op_dest];
                else
                    pc = pc + 1;
            end

            // 14. jmp if greater or equal
            4'b1110:
            begin
                if (register[op_left] <= ((is_op_right_const==1'b1) ? op_right : register[op_right[3:0]]))
                    pc = register[op_dest];
                else
                    pc = pc + 1;
            end

            // 15. jmp if lower  or equal
            4'b1111:
            begin
                if (register[op_left] >= ((is_op_right_const==1'b1) ? op_right : register[op_right[3:0]]))
                    pc = register[op_dest];
                else
                    pc = pc + 1;
            end
        endcase
    end

    assign vga_buffer_data = color;
    assign vga_buffer_address = address;


endmodule
