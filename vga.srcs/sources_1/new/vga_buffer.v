`timescale 1ns / 1ps

module vga_buffer(
    input wire clk,
    input wire [17:0] address_read,
    input wire [17:0] address_write,
    input wire [2:0] in_data,
    output wire [2:0] out_data
);

    parameter MEM_SIZE = 2 ** 18;
    reg [2:0] mem [MEM_SIZE-1:0];
    reg [2:0] d;

    always @(posedge clk) begin
        mem[address_write] <= in_data;
        d <= mem[address_read];
    end
    assign out_data = d;
endmodule