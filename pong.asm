// max display width 320
// max display height 480
// r15 is a special reg, containing the button states for player A
init:
mov r6 #1
mov r7 #-1
mov r8 #0 // how fast the speed growing
mov r9 #50 // player A pos
mov r10 #50 // player B pos
mov r11 #100 // ball poss x
mov r12 #100 // ball poss y
mov r13 #1 // ball speed x
mov r14 #1 // ball speed y

mov r5 #0

screen_saver:
// draw something
mov r2 #cycle_lost1
mov r3 #cycle_lost2
mov r0 #0 // y1
cycle_lost2:
mov r1 #0 // x1
cycle_lost1:
div r4 r0 #100
add r4 r4 r1
add r4 r4 r5
mod r4 r4 #8
mvb r0 r1 r4
add r1 r1 #1
jgr r2 r1 #320  // x2
add r0 r0 #1
jgr r3 r0 #480 // y2

add r5 r5 #1

mov r0 #next_frame
jeq r0 r15 #3
jmp #screen_saver

next_frame:
// draw player A
mov r2 #cycle_playerA1
mov r3 #cycle_playerA2
mov r0 r9 // y1
add r4 r9 #30 // y2
cycle_playerA2:
mov r1 #0 // x1
cycle_playerA1:
mvb r0 r1 #7
add r1 r1 #1
jgr r2 r1 #2 // x2
add r0 r0 #1
jgr r3 r0 r4


// draw player B
mov r2 #cycle_playerB1
mov r3 #cycle_playerB2
mov r0 r10 // y1
add r4 r10 #30 // y2
cycle_playerB2:
mov r1 #318 // x1
cycle_playerB1:
mvb r0 r1 #7
add r1 r1 #1
jgr r2 r1 #320  // x2
add r0 r0 #1
jgr r3 r0 r4


// draw ball
mov r2 #cycle_ball1
mov r3 #cycle_ball2
mov r0 r12 // y1
add r4 r12 #10  // y2
add r5 r11 #5  // x2  width
cycle_ball2:
mov r1 r11 // x1
cycle_ball1:
mvb r0 r1 #7
add r1 r1 #1
jgr r2 r1 r5
add r0 r0 #1
jgr r3 r0 r4

// wait
mov r2 #wait1
mov r3 #wait2
mov r0 #0
wait2:
mov r1 #0
wait1:
add r1 r1 #1
jgr r2 r1 #1000
add r0 r0 #1
jgr r3 r0 #50

////////////////////////////////////////
// blank player A
mov r2 #cycle_blank_playerA1
mov r3 #cycle_blank_playerA2
mov r0 r9 // y1
add r4 r9 #30 // y2
cycle_blank_playerA2:
mov r1 #0 // x1
cycle_blank_playerA1:
mvb r0 r1 #0
add r1 r1 #1
jgr r2 r1 #2 // x2
add r0 r0 #1
jgr r3 r0 r4


// blank player B
mov r2 #cycle_blank_playerB1
mov r3 #cycle_blank_playerB2
mov r0 r10 // y1
add r4 r10 #30 // y2
cycle_blank_playerB2:
mov r1 #318 // x1
cycle_blank_playerB1:
mvb r0 r1 #0
add r1 r1 #1
jgr r2 r1 #320  // x2
add r0 r0 #1
jgr r3 r0 r4


// blank ball
mov r2 #cycle_blank_ball1
mov r3 #cycle_blank_ball2
mov r0 r12 // y1
add r4 r12 #10  // y2
add r5 r11 #5  // x2
cycle_blank_ball2:
mov r1 r11 // x1
cycle_blank_ball1:
mvb r0 r1 #0
add r1 r1 #1
jgr r2 r1 r5
add r0 r0 #1
jgr r3 r0 r4

////////////////////////////////////////
// make the game harder as time goes on
add r8 r8 #1
mov r0 #hardness_cycle
jgr r0 r8 #5000
mov r8 #0
add r6 r6 #1 //ball direction
add r7 r7 #-1 //ball direction
hardness_cycle:

// ball physics
mov r0 #ball_not_left
jlo r0 r11 #2
mov r13 r6 // change ball direction to right
mov r1 #init
add r2 r12 #10
jgr r1 r2 r9
add r2 r9 #30
jgr r1 r2 r12
ball_not_left:
mov r0 #ball_not_right
jgr r0 r11 #318
mov r13 r7 // change ball direction to left
ball_not_right:

mov r0 #ball_not_bottom
jgr r0 r12 #475
mov r14 r7 // change ball direction to up
ball_not_bottom:
mov r0 #ball_not_up
jlo r0 r12 #2
mov r14 r6 // change ball direction to down
ball_not_up:


// player A control
mov r0 #paddle_move_down
mov r1 #paddle_no_move_down
jeq r0 r15 #1
jmp r1
paddle_move_down:
jlo r1 r9 #450
add r9 r9 #2
paddle_no_move_down:

mov r0 #paddle_move_up
mov r1 #paddle_no_move_up
jeq r0 r15 #2
jmp r1
paddle_move_up:
jgr r1 r9 #2
sub r9 r9 #2
paddle_no_move_up:

// "AI"
mov r0 #B_paddle_out_of_bound
jlo r0 r12 #445
mov r10 r12 // B player follows the ball
B_paddle_out_of_bound:

// move ball
add r11 r11 r13
add r12 r12 r14

jmp #next_frame

