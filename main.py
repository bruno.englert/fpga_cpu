# https://stackoverflow.com/questions/1604464/twos-complement-in-python
def twos_comp(val: int, bits: int):
    """compute the 2's complement of int value val"""
    if (val & (1 << (bits - 1))) != 0:  # if sign bit is set e.g., 8bit: 128-255
        val = val - (1 << bits)  # compute negative value
    return val & ((2 ** bits) - 1)  # return positive value as is


def is_int(s: str):
    try:
        int(s)
        return True
    except ValueError:
        return False


# if input is a positive integer we convert it to a positive integer
# if input is a negative integer we convert it to a negative integer using 2's complements
# if input is a label, we keep it as a string
def convert(s: str):
    if is_int(s):
        if int(s) > 0:
            return int(s)
        else:
            return twos_comp(int(s), 16) % (2 ** 16)
    else:
        return s


if __name__ == '__main__':
    with open("pong.asm") as f:
        content = f.readlines()
    content = [x.strip().split(" ") for x in content]

    out = []
    instructions = ['nop', 'mvb', 'mov', 'add',
                    'sub', 'mul', 'div', 'mod',
                    'sft', 'sfl', 'jmp', 'jeq',
                    'jgr', 'jlo', 'jeg', 'jel',
                    ]
    ctr = 0
    label_jumps = {}
    try:
        for c in content:
            if len(c) == 0:
                continue

            if c[0] not in instructions:
                if ':' in c[0]:
                    label_jumps[c[0][:-1]] = ctr
                elif "//" == c[0][:2]:
                    pass
            else:
                isnt = instructions.index(c[0])

                if c[0] == 'nop':
                    out.append((ctr, isnt, 0, 0, 0, 0))
                elif c[0] == 'mov':
                    is_right_op_const = "#" in c[2]
                    right_op = convert(c[2][1:])
                    dest_op = int(c[1][1:])

                    out.append((ctr, isnt, dest_op, 0, right_op, is_right_op_const))
                elif c[0] == 'jmp':
                    is_right_op_const = "#" in c[1]
                    right_op = convert(c[1][1:])

                    out.append((ctr, isnt, 0, 0, right_op, is_right_op_const))
                elif c[0] in ['mvb', 'add', 'sub', 'mul',
                              'div', 'mod', 'sft', 'sfl',
                              'jeq', 'jgr', 'jlo', 'jeg', 'jel']:
                    is_right_op_const = "#" in c[3]
                    right_op = convert(c[3][1:])
                    left_op = int(c[2][1:])
                    dest_op = int(c[1][1:])

                    out.append((ctr, isnt, dest_op, left_op, right_op, is_right_op_const))
                else:
                    raise ValueError(c[0] + ",  inst not implemented!")

                ctr += 1
    except Exception as e:
        print(str(e))
        print("Error:", c)
        exit(1)

    out_txt = ''
    for ctr, isnt_bin, dest_op, left_op, right_op, is_right_op_const in out:
        if type(right_op) == str:
            right_op = label_jumps[right_op]

        out_txt += '16\'b{:016b}: data = 29\'b{:04b}{:04b}{:04b}{:016b}{:01b};'.format(
            ctr, isnt_bin, dest_op, left_op, right_op, is_right_op_const) + '\n'
    print(out_txt)
